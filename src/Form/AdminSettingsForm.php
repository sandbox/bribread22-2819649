<?php

namespace Drupal\newsfeed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form class for the Newsfeed module.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'newsfeed_adminsettingsform';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'newsfeed.settings',
    ];
  }

  /**
   * Form constructor.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('newsfeed.settings');

    $form['feed_list'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Enter list of feeds'),
      '#description' => $this->t('Enter in the list of feeds separated by a new line.'),
      '#default_value' => $config->get('feed_list'),
    );

    $form['add_content'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Add content?'),
      '#description' => $this->t('Check this box if you want the content extracted from the feed to be saved to the database as an Article.'),
      '#default_value' => $config->get('add_content'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('newsfeed.settings')
      ->set('feed_list', $form_state->getValue('feed_list'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
