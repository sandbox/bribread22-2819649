<?php

/**
 * @file
 * Contains \Drupal\newsfeed\Form\NewsfeedConfigurationForm.
 */

namespace Drupal\newsfeed\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for performing a 1-click site backup.
 */
class NewsfeedConfigurationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'newsfeed_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();

    $max_sites = 10;

    for ($i = 0; $i < $max_sites; $i++) {
      $site_key = 'site_' . ($i + 1);
      $form[$site_key] = array(
      	'#type' => 'fieldset',
      	'#title' => t('Site #@site_no', array('@site_no' => ($i + 1))),
      	'#collapsible' => TRUE,
      	'#collapsed' => FALSE,
      );
      $form[$site_key]['url'] = array(
      	'#type' => 'textfield',
      	'#title' => t('URL'),
      	'#description' => t('Enter the URL of the site.'),
      );
      $form[$site_key]['title_selector'] = array(
      	'#type' => 'textfield',
      	'#title' => t('Title Selector'),
      	'#description' => t('Enter the css selector for the title.'),
      );
      $form[$site_key]['content_selector'] = array(
      	'#type' => 'textfield',
      	'#title' => t('Content Selector'),
      	'#description' => t('Enter the css selector for the content.'),
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}
