<?php
/**
 * Created by PhpStorm.
 * User: bmcveigh
 * Date: 10/13/2016
 * Time: 11:25 AM
 */

namespace Drupal\newsfeed;


class Newsfeed {
  private $query;

  /**
   * NewsFeed constructor.
   * @param $query
   */
  public function __construct($query = 'Donald%20Trump') {
    $this->query = $query;
  }

  /**
   * @return mixed
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * @param mixed $query
   */
  public function setQuery($query) {
    $this->query = $query;
  }

  /**
   * Find the available newsfeeds from Google based on the query.
   *
   * @return mixed
   */
  public function findFeeds() {
    $url = 'https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=' . $this->query;
    $client = \Drupal::httpClient();
    $request = $client->request('GET', $url);
    $json = $request->getBody();

    return json_decode($json);
  }

  /**
   * Get the 10 latest entries from Google based on the query.
   * 
   * @return array
   *   The results from the GET request from Google.
   */
  public function getEntries() {
    return $this->findFeeds()->responseData->entries;
  }

  /**
   * Load in the results of a specific feed.
   *
   * @param $url
   * @return mixed
   */
  public function loadFeed($url) {
    // Example: http%3A%2F%2Fwww.digg.com%2Frss%2Findex.xml
    $url = 'https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=' . urlencode($url);
    $client = \Drupal::httpClient();
    $request = $client->request('GET', $url);
    $json = $request->getBody();

    return json_decode($json);
  }

}
